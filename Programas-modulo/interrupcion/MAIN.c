//configuracion de cristal y frecuencia
#pragma config WDT = OFF 
#pragma config LVP = OFF
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON

#pragma config MCLRE = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"

uint8_t USB_Out_Buffer[64];
const char buffer[64];
int cont=0;
void USBTask(void);
void SYSTEM_Initialize(void);
void Configuracion(void);

void main(void) {
  
    SYSTEM_Initialize();//inicializacion de todo el sistema
    while (1)
    {
       USBTask(); 
       __delay_ms(2);
    }
}
void USBTask(void){
    uint8_t READ;
    uint8_t n;
    n=64;
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexión
    
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {
        READ = getsUSBUSART(USB_Out_Buffer,n);
        if(READ != 0){
            
            putrsUSBUSART(USB_Out_Buffer); //escribir 
        }
    }
    CDCTxService(); //Procesa servicio USB
}
void SYSTEM_Initialize(void){
    
    Configuracion();
     
    USBDeviceInit();
    USBDeviceAttach();
}
void Configuracion(void){
//realizar configuración de registros TRIS 
    TRISC0=0;
    LATC0=1;
    GIE=1;
    PEIE=1;
    USBIE=1;
}
void __interrupt() INTERRUPT_InterruptManager (void)
{
    // interrupt handler
    if (USBIF == 1) {
        USBIF=0;
        USBDeviceTasks();
        if(UEIR!=0){
           UEIR=0; 
        }
        if(UIR!=0){
           UIR=0; 
        }
    }
}
