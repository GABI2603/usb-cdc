//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON
#pragma config LVP = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include<math.h>
#include "usb.h"
#include "usb_device_cdc.h"

char USB_Out_Buffer[64];
char buffer[64];
float angulo=0;
int cmp=0;
void USBTask(void);
float strtofloat(char *data);
int MoverServo(float info);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);
void PIN_MANAGER_IOC(void);
void Configuracion(void);
void __interrupt() TIMER(void);

void main(void) {
    
    SYSTEM_Initialize();//inicializacion de todo el sistema
    TMR1ON=1;  
    while (1)
    {        
       USBDeviceTasks();
       USBTask(); 
    }
}
void USBTask(void){
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexión
    uint8_t READ = getsUSBUSART(USB_Out_Buffer,64);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {        
        if(READ != 0){
            angulo = strtofloat(USB_Out_Buffer);
            cmp = MoverServo(angulo);
           
        }
    }
    CDCTxService(); //Procesa servicio USB
    
}
float strtofloat(char *data){
    float resultado;
    int i, conv, n, punto=1,cont=0, decimales=0, enteros=0;
    n=strlen(data);
    for(i=0;i<n;i++){
        conv=(int)data[i];
        if((conv<=57)&&(conv>=48)){
            if(punto==1){
            conv=(int)data[i]-48;
            enteros=(enteros*10)+conv;
            }
            else{
                conv=(int)data[i]-48;
                decimales=(decimales*10)+conv;
                cont++;
            }
        }
        else{
            if((conv==46)&& (punto==1)){
                punto=0;
            }
            else{
                break;
            }  
        }
    }
    resultado=(float)enteros+((float)decimales/(pow(10,cont)));
    return resultado;
}
int MoverServo(float info){//ACOMODAR AL PWM NECESARIO
    float pwm; 
    //conversion a pwm
    if(info>180){
        info=180;
    }
    if(info<0){
        info=0;
    }
    pwm=info*3000/180;
    
    return (int)pwm;
}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    USBDeviceInit();
}
void  INTERRUPT_Initialize (void){

    GIE = 1;//Activar interrupciones globales
     //TIMER0
    T0CON = 0b10000111; 
    TMR0L = 0x1A; 
    TMR0H = 0xB7; 
    TMR0IE = 1;//Activa interrupcion timer
    TMR0ON = 1;//activa timer
    //CCP
    PEIE=1;
    TMR1IF=0;
    TMR1IE=1;
    
 }
void Configuracion(void){
//realizar configuración de registros TRIS
    TRISB7=0;//led timer
    LATB7=0;//iniciar leds
    TRISC2=0;//CCP1
    //CCP
    CCP1CON=0b00001001;
    T1CON=0xF8;
    TMR1=43036;//35536;//60536;
    CCPR1=35536+750+cmp;//35536+3750;
     
  
//INTCON2bits.nRBPU = 1;
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0x1A; 
        TMR0H = 0xB7; 
        TMR0IF = 0;//desactiva bandera
        LATB7 = LATB7^1;//cambio estado del led
    }
    if(TMR1IF==1){
        TMR1IF=0;
        TMR1=35536;
        CCPR1=35536+750+cmp;
        CCP1CON=0b00001001;
    }
}
