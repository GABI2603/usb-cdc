//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON
#pragma config LVP = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"
#include "usb_device_cdc.h"

char USB_Out_Buffer[64];
char buffer[64];
int n=0;
void USBTask(void);
void USB_ENVIO(char *data);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);
void Configuracion(void);
void RS232_recepcion(char *almacenamiento);
void RS232_transmision(char *almacenamiento);
void __interrupt() TIMER(void);

void main(void) {
    
    SYSTEM_Initialize();//inicializacion de todo el sistema
    
    while (1)
    {  
       if(n>0){
            USB_ENVIO(buffer);
            n=0;
       }
       USBDeviceTasks();
       USBTask(); 
       
    }
}
void USBTask(void){
    int num,t;
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexión
    uint8_t READ = getsUSBUSART(USB_Out_Buffer,64);
    t=strlen(USB_Out_Buffer);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {        
        if(READ != 0){
            RS232_transmision(USB_Out_Buffer);
        }
    }
    
    CDCTxService(); //Procesa servicio USB
    
}
void USB_ENVIO(char *data){
    bool envio=1;
    do{
        USBDeviceTasks();
        if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexión
        if(USBUSARTIsTxTrfReady()){
            putrsUSBUSART(data); //enviar dato USB
            envio=0;
        }
        CDCTxService(); //Procesa servicio USB
    }while(envio==1);
}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    USBDeviceInit();
}
void  INTERRUPT_Initialize (void){
	
    GIE = 1;//Activar interrupciones globales
     //TIMER0
    T0CON = 0b10000110; // preescaler 128
    TMR0L = 0x1A;
    TMR0H = 0x10;
    TMR0IE = 1;//Activa interrupcion timer
    TMR0ON = 1;//activa timer
    //rs232
    PEIE = 1;
    RCIE = 1;
 }
void Configuracion(void){
//realizar configuración de registros TRIS
    TRISC = 0x80;//USART
    LATC0=0;
    //Configuracion USART
   SPBRG = 0x4D;
   RCSTA = 0x90;
   TXSTA = 0x20;
    
//INTCON2bits.nRBPU = 1;
}
void RS232_transmision(char *almacenamiento){
    int i,n;
    n=strlen(almacenamiento);
    for(i=0;i<n;i++){
            while(TXIF==0);
            TXREG=almacenamiento[i];
        }
    __delay_ms(10);
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0x1A;
    TMR0H = 0x10;
        TMR0IF = 0;//desactiva bandera
        LATC0 = LATC0^1;//cambio estado del led
    }
    if(RCIF==1){
        RCIF=0;
        buffer[n]=RCREG;
        n++;
    }
}
