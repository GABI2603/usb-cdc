//configuracion de cristal y frecuencia
/*#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON

#define _XTAL_FREQ 48000000*/
#pragma config FOSC = HS //20MHz
#pragma config WDT = OFF
#pragma config LVP = OFF
#define _XTAL_FREQ 20000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>

int ref=20;
int data=0, lectura=0, refmin, refmax;
void SYSTEM_Initialize(void);
void Configuracion(void);
void  INTERRUPT_Initialize(void);
float referencia(void);
float TEMP(void);

#define DATA_DIR TRISC1
#define DATA_IN RC1
#define DATA_OUT LATC1
unsigned char Temp,Hum,Che;
unsigned char Dec,Uni;
void LeerHT11(void);
unsigned char LeerByte(void);
unsigned char LeerBit(void);
unsigned char Check();

void main(void) {
     SYSTEM_Initialize();//inicializacion de todo el sistema
     
     while (1){
        __delay_ms(1000);
        ref=referencia();
        refmin=ref-3;
        refmax=ref+3;
        __delay_ms(2);
        LATB7=1;
        lectura=TEMP();
        LATB7=0;
        
        if(lectura > refmin){
            if(lectura > refmax){
                LATD0=0;//APAGAR BOMBILLO
            }
        }
        else{            
            LATD0=1;//ENCENDER BOMBILLO
        }
     }
    
}
void SYSTEM_Initialize(void){
    Configuracion();//PIN_MANAGER_Initialize();
    INTERRUPT_Initialize();
}
void Configuracion(void){
    TRISB7=0;//SALIDA RELE
    TRISC0=0;//led timer
    LATB7=0;
    LATC0=0;//iniciar led
    TRISD=0;//BOMBILLO apagado
    //configuracion ADC
    ADCON1=0x0D;//definir puertos analogos
    ADCON2=0xA2;//conversor analogo-digital
    DATA_OUT=0;
}
void  INTERRUPT_Initialize (void){
    GIE = 1;//Activar interrupciones globales
    //TIMER0
    T0CON = 0b10000111;
    TMR0L = 0x1A; 
    TMR0H = 0xB7; 
    TMR0IE = 1;//Activa interrupcion timer
}
float referencia(void){//entre 20�C y 40�C
    unsigned int valor;
    float salida;
    //lectura ADC
    ADCON0=0x03;
    while(ADCON0bits.GO_nDONE == 1);
    valor = ((ADRESH<<8)|ADRESL);
    //CONVERSION 
    salida = (valor*20/1023)+20;
    return salida;   
}
float TEMP(void){
    float salida=0;
    LeerHT11();
    salida=Temp;
    return salida;
}
void LeerHT11(void){
  unsigned char i,contr=0;
  DATA_DIR=0;
  __delay_ms(18);
  DATA_DIR=1;
  while(DATA_IN==1);
  __delay_us(40);
  if(DATA_IN==0) contr++;
  __delay_us(80);
  if(DATA_IN==1) contr++;
  while(DATA_IN==1);
  Hum=LeerByte();
  LeerByte();
  Temp=LeerByte();
  LeerByte();
  Che=LeerByte();
}
unsigned char LeerByte(void){
  unsigned char res=0,i;
  for(i=8;i>0;i--){
    res=(res<<1) | LeerBit();  
  }
  return res;
}
unsigned char LeerBit(void){
  unsigned char res=0;
  while(DATA_IN==0);
  __delay_us(13);
  if(DATA_IN==1) res=0;
  __delay_us(22);
  if(DATA_IN==1){
    res=1;
    while(DATA_IN==1);
  }  
  return res;  
}
unsigned char Check(void){
  unsigned char res=0,aux;
  aux=Temp+Hum;
  if(aux==Che) res=1;
  return res;  
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0x1A; 
        TMR0H = 0xB7; 
        TMR0IF = 0;//desactiva bandera
        LATC0=LATC0^1;
    } 
}
