//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON

#pragma config LVP = OFF
#pragma config MCLRE = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"

uint8_t USB_Out_Buffer[64];
char buffer[64];
int ref=20, refmin=17, refmax=23;
int data=0, lectura=0;


//void ProcessIO(void);
void USBTask(void);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);
int strtoint(char *valor);
void Configuracion(void);

float TEMP(void);

#define DATA_DIR TRISC1
#define DATA_IN RC1
#define DATA_OUT LATC1
unsigned char Temp,Hum,Che;
unsigned char Dec,Uni;
void LeerHT11(void);
unsigned char LeerByte(void);
unsigned char LeerBit(void);
unsigned char Check();

void main(void) {
  
    SYSTEM_Initialize();//inicializacion de todo el sistema
    while (1)
    {
        __delay_ms(1000);
        USBTask();
        LATB7=1;
        lectura=TEMP();
        LATB7=0;
        
        if(lectura > refmin){
            if(lectura > refmax){
                LATD0=0;//APAGAR BOMBILLO
            }
        }
        else{            
            LATD0=1;//ENCENDER BOMBILLO
        }
       
    }
}
void USBTask(void){
    uint8_t READ;
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexi�n
    READ = getsUSBUSART(USB_Out_Buffer,64);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {
        if(READ != 0){
            
                ref=strtoint(USB_Out_Buffer);
                refmin=ref-3;
                refmax=ref+3;              
            
        }
        
        
    }
    CDCTxService(); //Procesa servicio USB
}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    USBDeviceInit();
    USBDeviceAttach();

}
void  INTERRUPT_Initialize (void){
    GIE = 1;//Activar interrupciones globales
    //TIMER0
    T0CON = 0b10000111; 
    TMR0L = 0x1A; 
    TMR0H = 0xB7;
    TMR0IE = 1;//Activa interrupcion timer
    //USB
    PEIE=1;
    USBIE=1;
}
void Configuracion(void){
    TRISD0=0;//SALIDA RELE
    TRISC0=0;//led timer
    TRISB7=0;
    LATB7=0;
    LATD0=0;//BOMBILLO apagado
    LATC0=0;//iniciar led
    TRISA0=0;
    LATA0=0;
    //configuracion ADC
    ADCON1=15;//definir puertos DIGITALES
    DATA_OUT=0;

}
float TEMP(void){
    float salida=0;
    __delay_ms(500);

    __delay_ms(500);

    LeerHT11();
    salida=Temp;
    return salida;
}
void LeerHT11(void){
  unsigned char i,contr=0;
  DATA_DIR=0;
  __delay_ms(18);
  DATA_DIR=1;
  while(DATA_IN==1);
  __delay_us(40);
  if(DATA_IN==0) contr++;
  __delay_us(80);
  if(DATA_IN==1) contr++;
  while(DATA_IN==1);
  Hum=LeerByte();
  LeerByte();
  Temp=LeerByte();
  LeerByte();
  Che=LeerByte();
}
unsigned char LeerByte(void){
  unsigned char res=0,i;
  for(i=8;i>0;i--){
    res=(res<<1) | LeerBit();  
  }
  return res;
}
unsigned char LeerBit(void){
  unsigned char res=0;
  while(DATA_IN==0);
  __delay_us(13);
  if(DATA_IN==1) res=0;
  __delay_us(22);
  if(DATA_IN==1){
    res=1;
    while(DATA_IN==1);
  }  
  return res;  
}
unsigned char Check(void){
  unsigned char res=0,aux;
  aux=Temp+Hum;
  if(aux==Che) res=1;
  return res;  
}
int strtoint(char *valor){
    int i,resultado,conv,n;
    n=strlen(valor);
    resultado=0;
    for(i=0;i<n;i++){
        conv=(int)valor[i];
        if((conv<=57)&&(conv>=48)){
            conv=(int)valor[i]-48;
            resultado=(resultado*10)+conv;
        }
        else{
            break;
        }
    }
    return resultado;
}
void __interrupt() INTERRUPT_InterruptManager (void)
{
    // interrupt handler
    if (USBIF == 1) {
        USBIF=0;
        USBDeviceTasks();
        /*if(UEIR!=0){
           UEIR=0; 
        }
        if(UIR!=0){
           UIR=0; 
        }*/
        LATA0=1;
    }
    if (TMR0IF == 1) {
        TMR0L = 0x1A; 
        TMR0H = 0xB7;
        TMR0IF = 0;//desactiva bandera
        LATC0=LATC0^1;
    } 
}
