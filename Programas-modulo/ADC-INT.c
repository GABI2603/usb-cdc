//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON
#pragma config LVP = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"
#include "usb_device_cdc.h"

char USB_Out_Buffer[64];
char buffer[64];
int cont=0, muestra=0;
bool flag=0, envio=0;
void ProcessIO(void);
void USBTask(void);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);

void Configuracion(void);
float ADC(void);
void __interrupt() TIMER(void);

void main(void) {
    
    SYSTEM_Initialize();//inicializacion de todo el sistema
    
    while (1)
    {
       
       USBDeviceTasks();
       USBTask();
       
    }
}
void USBTask(void){
    float lectura=0;
     if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexión
     uint8_t READ = getsUSBUSART(USB_Out_Buffer,64);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {        
        if(READ != 0){
            if(USB_Out_Buffer[0]==0x43){
                muestra=1;
                flag=1;                     
            }
        }
        if(flag==1){
            lectura=ADC();
            sprintf(buffer,"%d, %.2f \n",muestra,lectura);//Convertir dato a string
            putrsUSBUSART(buffer); //enviar dato USB
            __delay_ms(200);
            muestra++;
        }
        else{
            if(envio==1){
                envio=0;
                putrsUSBUSART("F \n"); //enviar dato USB
                __delay_ms(400);
            }
        }
        
    }
    CDCTxService(); //Procesa servicio USB
}
    

void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    USBDeviceInit();
}
void  INTERRUPT_Initialize (void){
    GIE = 1;//Activar interrupciones globales
     //TIMER0
    T0CON = 0b10000110; 
    TMR0L = 0x1A; 
    TMR0H = 0x10; 
    TMR0IE = 1;//Activa interrupcion timer
    TMR0ON = 1;//activa timer
    TMR0IF = 1;//Activar bandera
    //int0
    PEIE=1;//INTERRUPCION PERIFERICA
    INT0IE=1;//INTERRUPCION INT0
    INT0IF=1;//DESACTIVAR BANDERA
}
void Configuracion(void){
//realizar configuración de registros TRIS
    TRISB=0x1;// int0
    LATB=0;//int0
    TRISC0=0;
    LATC0=0;
    //configuracion ADC
    ADCON1=0x0D;//definir puertos analogos
    ADCON2=0xA2;//conversor analogo-digital
  

}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0x1A; 
        TMR0H = 0x10; 
        TMR0IF = 0;//desactiva bandera
        LATC0 = LATC0^1;//cambio estado del led
    }
    if(INT0IF == 1){
        INT0IF=0;//desactivar la bandera
        flag=0;//detener envio de datos
        envio=1;
    }
}
float ADC(void){
    unsigned int valor;
    float salida;
    //lectura ADC
    ADCON0=0x07;
    while(ADCON0bits.GO_nDONE == 1);
    valor = ((ADRESH<<8)|ADRESL);
    //CONVERSION
    salida = ((float)valor) * 5 / 1023;
    return salida;    
}
