//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON

#pragma config LVP = OFF
#pragma config MCLRE = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"

void SYSTEM_Initialize(void);
void Configuracion(void);
void USBTask(void);
int strtoint(char *valor);
void  INTERRUPT_Initialize(void);
float referencia(void);
bool flag=0, time=0;
uint8_t USB_Out_Buffer[64];
char buffer[64];
int ref=20, data=0;
int cont=0, lectura=0,error=0,derror=0,errora=0;
int temperatura[3];

void Fuzzificacion(float T, float dT);
void AnalisisReglas(float T);
float Defuzzificacion(void);

//derivada error
float dnegativo(float T);
float dneutro(float T);
float dpositivo(float T);

//error
float negativo(float T);
float neutro(float T);
float positivo(float T);


float duf,dut,duc,uf,ut,uc,r1,r2,r3,sal;
float reglas[9]={0,0,0,0,0,0,0,0,0};
int temperatura[3];
int seleccion=20;

float TEMP(void);

#define DATA_DIR TRISC1
#define DATA_IN RC1
#define DATA_OUT LATC1
unsigned char Temp,Hum,Che;
unsigned char Dec,Uni;

void LeerHT11(void);
unsigned char LeerByte(void);
unsigned char LeerBit(void);
unsigned char Check();
int mediana(int *datos);
void main(void) {
     SYSTEM_Initialize();//inicializacion de todo el sistema
     __delay_ms(500);
     while(1){
        __delay_ms(500);
        LATC0=1;
        temperatura[0]=TEMP();
        __delay_ms(250);
        temperatura[1]=TEMP();
        __delay_ms(250);
        temperatura[2]=TEMP();
        lectura=mediana(temperatura);
        error=ref-lectura;//calculo del error
        derror=error-errora;
        errora=error;
        Fuzzificacion(error,derror);
        //se calcula el valor de activaci�n de cada regla
        AnalisisReglas(error);
        //se calcula el valor de salida por el metodo de momento medio MOM
        sal=20 * Defuzzificacion();//la fuzificacion se multiplica por 20 para conocer el valor en el cual se activa el pulso
        seleccion=(int)sal;
        USBTask();
        LATC0=0;
        
     }
}
void USBTask(void){
    
    uint8_t READ;
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexi�n
    READ = getsUSBUSART(USB_Out_Buffer,64);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {
        
        if(READ != 0){
            if(USB_Out_Buffer[0]=='A'){
               sprintf(buffer," %d %d %d ", lectura,seleccion,ref);
               putrsUSBUSART(buffer); 
            }
            else{
                ref=strtoint(USB_Out_Buffer);
            }
                 
        }
        
    }
    CDCTxService(); //Procesa servicio USB
}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    USBDeviceInit();
    USBDeviceAttach();
}
void Configuracion(void){
    TRISD7=0;//SALIDA RELE
    TRISD0=0;
    TRISC0=0;//led timer
    TRISB0=1;
    TRISB7=0;
    LATB7=0;
    LATD7=0;//BOMBILLO apagado
    LATC0=0;//iniciar led
    LATD0=0;
    DATA_OUT=0;
    //configuracion ADC
    ADCON1=15;//conversor analogo-digital
}
void  INTERRUPT_Initialize (void){
    GIE = 1;//Activar interrupciones globales
    //TIMER0
    T0CON = 0b10000101; 
    TMR0L = 0x5A; 
    TMR0H = 0xFF; 
    TMR0IE = 1;//Activa interrupcion timer
    //int0
    PEIE=1;//INTERRUPCION PERIFERICA
    INT0IE=1;//INTERRUPCION INT0
    //USB
    USBIE=1;
}

float dnegativo(float T){
    float grado;
    if(T<-2){
      grado=1;    
    }else if(T<0){
      grado=-(T/2);  
    }else{
      grado=0;  
    }
    return grado;
}
float dneutro(float T){
    float grado;
    if(T<-2){
        grado=0;
    }else if(T<0){
        grado=1+(T/2);
    }else if(T<2){
        grado=1-(T/2);
    }
    else{
        grado=0;
    }
    return grado;
}
float dpositivo(float T){
    float grado;
    if(T<0){
      grado=0;    
    }else if(T<2){
      grado= (T/2);  
    }else{
      grado=1;  
    }
    return grado; 
}
float negativo(float T){
    float grado;
    if(T<-4){
      grado=1;    
    }else if(T<0){
      grado=1-(T/4);  
    }else{
      grado=0;  
    }
    return grado;
}
float neutro(float T){
    float grado;
    if(T<-4){
        grado=0;
    }else if(T<0){
        grado=2+(T/4);
    }else if(T<4){
        grado=2-(T/4);
    }
    else{
        grado=0;
    }
    return grado;
}
float positivo(float T){
    float grado;
    if(T<0){
      grado=0;    
    }else if(T<4){
      grado= 1+(T/4);  
    }else{
      grado=1;  
    }
    return grado; 
}
void Fuzzificacion(float T, float dT){
  //Funci�n que concentra el calculo de los grados de pertenencia
  uc=negativo(T);
  ut=neutro(T);
  uf=positivo(T);
  
  duc=dnegativo(dT);
  dut=dneutro(dT);
  duf=dpositivo(dT);
}
void AnalisisReglas(float T){
  //Funci�n que calcula el valor de activaci�n de las reglas
    for(int i=0; i<9; i++){
        reglas[i]=0;
    }
  if((uf>0)&&(duf>0)){
      if(uf<duf){
          reglas[0]=uf;
      }
      else{
          reglas[0]=duf;
      }
  }
  if((uf>0)&&(dut>0)){
      if(uf<dut){
          reglas[1]=uf;
      }
      else{
          reglas[1]=dut;
      }
  }
  if((ut>0)&&(duf>0)){
      if(ut<duf){
          reglas[2]=ut;
      }
      else{
          reglas[2]=duf;
      }
  }
  if((uf>0)&&(duc>0)){
      if(uf<duc){
          reglas[3]=uf;
      }
      else{
          reglas[3]=duc;
      }
  }
  
  if((ut>0)&&(dut>0)){
      if(ut<dut){
          reglas[4]=ut;
      }
      else{
          reglas[4]=dut;
      }
  }
  if((uc>0)&&(duf>0)){
      if(uc<duf){
          reglas[5]=uc;
      }
      else{
          reglas[5]=duf;
      }
  }
  if((ut>0)&&(duc>0)){
      if(ut<duc){
          reglas[6]=ut;
      }
      else{
          reglas[6]=duc;
      }
  }
  if((uc>0)&&(dut>0)){
      if(uc<dut){
          reglas[7]=uc;
      }
      else{
          reglas[7]=dut;
      }
  }
  if((uc>0)&&(duc>0)){
      if(uc<duc){
          reglas[8]=uc;
      }
      else{
          reglas[9]=duc;
      }
  }  
}
float Defuzzificacion(void){//los valores de defizificacion varian entre 0 y 1 que al ser multiplicados por 20 da un valor entero entre 0 y 20
  //Funci�n que calcula el valor de salida apartir de los centroides de los conjuntos de salida
  float sal=0, max=0;  
  if((reglas[0]>0)||(reglas[1]>0)||(reglas[2]>0)){
      max=reglas[0];
      for(int i=0; i<3 ; i++){
          if(reglas[i]>max){
              max=reglas[i];
          }
      }
      sal=sal+0.04*max;
  }
  if((reglas[3]>0)||(reglas[4]>0)||(reglas[5]>0)){
      max=reglas[3];
      for(int i=0; i<3 ; i++){
          if(reglas[i+3]>max){
              max=reglas[i+3];
          }
      }
      sal=sal+0.85*max;
  }
    
  if((reglas[6]>0)||(reglas[7]>0)||(reglas[8]>0)){
      max=reglas[6];
      for(int i=0; i<3 ; i++){
          if(reglas[i+6]>max){
              max=reglas[i+6];
          }
      }
      sal=sal+0.96*max;
  }  
  return sal;
}

float TEMP(void){
    float salida=0;    
    LeerHT11();
    salida=Temp;
    return salida;
}
void LeerHT11(void){
  unsigned char i,contr=0;
  DATA_DIR=0;
  __delay_ms(18);
  DATA_DIR=1;
  while(DATA_IN==1);
  __delay_us(40);
  if(DATA_IN==0) contr++;
  __delay_us(80);
  if(DATA_IN==1) contr++;
  while(DATA_IN==1);
  Hum=LeerByte();
  LeerByte();
  Temp=LeerByte();
  LeerByte();
  Che=LeerByte();
}
unsigned char LeerByte(void){
  unsigned char res=0,i;
  for(i=8;i>0;i--){
    res=(res<<1) | LeerBit();  
  }
  return res;
}
unsigned char LeerBit(void){
  unsigned char res=0;
  while(DATA_IN==0);
  __delay_us(13);
  if(DATA_IN==1) res=0;
  __delay_us(22);
  if(DATA_IN==1){
    res=1;
    while(DATA_IN==1);
  }  
  return res;  
}
unsigned char Check(void){
  unsigned char res=0,aux;
  aux=Temp+Hum;
  if(aux==Che) res=1;
  return res;  
}
int strtoint(char *valor){
    int i,resultado,conv,n;
    n=strlen(valor);
    resultado=0;
    for(i=0;i<n;i++){
        conv=(int)valor[i];
        if((conv<=57)&&(conv>=48)){
            conv=(int)valor[i]-48;
            resultado=(resultado*10)+conv;
        }
        else{
            break;
        }
    }
    return resultado;
}
int mediana(int *datos){
    int salida, cambio, n, x;
    n=strlen(datos);
    for(int i=0; i<(n-1);i++){
        for(int j=0; j<n; j++){
            if(datos[i]>datos[j]){
                cambio=datos[i];
                datos[i]=datos[j];
                datos[j]=cambio;
            }
        }
    }
    x=n/2;
    salida=datos[x];
    return salida;
}
void __interrupt() INTERRUPT_InterruptManager (void){
    if (USBIF == 1) {
        USBIF=0;
        USBDeviceTasks();
        /*if(UEIR!=0){
           UEIR=0; 
        }
        if(UIR!=0){
           UIR=0; 
        }*/
    }
    if (TMR0IF == 1) {//el timer tiene un tiempo de 1/20 del periodo de la red 
        if(seleccion<19){//si el valor de seleccion es 20 (el maximo) el bombillo permanece apagado
            if(cont==seleccion){//define en que pulso se enciende el bombillo
                LATD7=1;
            }
        }
        TMR0L = 0x5A; 
        TMR0H = 0xFF; 
        TMR0IF = 0;//desactiva bandera
        cont++;//se lleva el conteo de interrupciones
    }
    
     if(INT0IF == 1){//lee el valor de cruce por cero
        INT0IF=0;//desactivar la bandera
        
        flag = flag ^ 1 ;//se determina la segunda vez que la se�al pasa por 0
        if (flag==1){//cuando la se�al pasa por 0 por primera vez
            TMR0IF = 1;//se activa la bandera del timer
            cont=0;//se reinicia el conteo
            if(seleccion != 0){//si seleccion =0 el bombillo esta encendido todo el tiempo
                LATD7=0;
            }
        }
    }
}
