//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON
#pragma config LVP = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"
#include "usb_device_cdc.h"

char USB_Out_Buffer[64];
char buffer[64], donne[64];
bool envio=0,accion=0;
unsigned char registre=0, active=0;
void reset_donne(void);
int i, cant=0;
int strtoint(char *valor);
void EscribirEEPROM(unsigned char registro, unsigned char dato);
unsigned char LeerEEPROM(unsigned char registro);
void USBTask(void);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);
void Configuracion(void);
void __interrupt() TIMER(void);

void main(void) {
    
    SYSTEM_Initialize();//inicializacion de todo el sistema
    active=0;
    while (1)
    {
       USBDeviceTasks();
       USBTask();
       
    }
}
void USBTask(void){
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexión
    uint8_t READ = getsUSBUSART(USB_Out_Buffer,64);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {        
        if(READ != 0){
            if(active==2){
                active=0;
                cant=0;
                if(accion==1){//escritura
                    cant=strlen(USB_Out_Buffer);
                    if(cant>256){cant=256;}
                    for(i=0;i<cant;i++){
                        EscribirEEPROM(registre, USB_Out_Buffer[i]);
                        registre++;
                    }
                    CDCTxService();
                    USBDeviceTasks();
                    putrsUSBUSART("Dato guardado \r\n");                           
                }
                else{//lectura
                    cant=strtoint(USB_Out_Buffer);
                    if(cant>255){cant=255;}
                    if(cant<=0){cant=1;}
                    reset_donne();
                    for(i=0;i<cant;i++){
                        donne[i]=LeerEEPROM(registre);
                        registre++;
                    }
                    sprintf(buffer,"El dato es: %s \r\n",donne);
                    CDCTxService();
                    USBDeviceTasks();
                    putrsUSBUSART(buffer);
                }
            }
            if(active==1){
                active=2;
                registre=strtoint(USB_Out_Buffer);
                if(accion==1){//escritura
                    CDCTxService();
                    USBDeviceTasks();
                    putrsUSBUSART("Ingrese el dato a guardar: \r\n");
                }
                else{//lectura
                    CDCTxService();
                    USBDeviceTasks();
                    putrsUSBUSART("Ingrese la cantidad de datos a leer(1 a 256): \r\n");   
                }
            }
            if(((USB_Out_Buffer[1]==0x45)||(USB_Out_Buffer[0]==0x45))&&(active==0)){//E
                active=1;
                accion=1;
                CDCTxService();
                USBDeviceTasks();
                putrsUSBUSART("Ingrese la direccion del registro donde desea escribir(0 a 255): \r\n");
            }
            if(((USB_Out_Buffer[1]==0x4C)||(USB_Out_Buffer[0]==0x4C))&&(active==0)){//L
                active=1;
                accion=0;
                CDCTxService();
                USBDeviceTasks();
                putrsUSBUSART("Ingrese la direccion del registro a leer(0 a 255): \r\n"); 
            }
        }
    }
    CDCTxService(); //Procesa servicio USB
    
}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    USBDeviceInit();
}
void  INTERRUPT_Initialize (void){

    GIE = 1;//Activar interrupciones globales
     //TIMER0
    T0CON = 0b10000111; // preescaler 256
    TMR0L = 0x1A; 
    TMR0H = 0xB7; 
    TMR0IE = 1;//Activa interrupcion timer
    TMR0ON = 1;//activa timer

 }
void Configuracion(void){
//realizar configuración de registros TRIS
    TRISC0=0;//led timer
    LATC0=0;//iniciar led
  
INTCON2bits.nRBPU = 1;
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0x1A; 
        TMR0H = 0xB7; 
        TMR0IF = 0;//desactiva bandera
        LATC0 = LATC0^1;//cambio estado del led
    }
}

int strtoint(char *valor){
    int i,resultado, conv, n;
    n=strlen(valor);
    resultado=0;
    for(i=0;i<n;i++){
        conv=(int)valor[i];
        if((conv<=57)&&(conv>=48)){
            conv=(int)valor[i]-48;
            resultado=(resultado*10)+conv;
        }
        else{
            break;
        }
    }
    return resultado;
}
void EscribirEEPROM(unsigned char registro, unsigned char dato){
  EEADR=registro;
  EEDATA=dato;
  EEPGD=0;
  CFGS=0;
  WREN=1;
  GIE=0;
  EECON2=0x55;
  EECON2=0xAA;
  WR=1;
  while(WR==1);
  GIE=1;
  WREN=0;
}
unsigned char LeerEEPROM(unsigned char registro){
  EEADR=registro;
  EEPGD=0;
  CFGS=0;
  RD=1;
  return EEDATA;
}
void reset_donne(void){
    int len=0,j;
    len=strlen(donne);
    for(j=0;j<len;j++){
        donne[j]=0;
    }
}
