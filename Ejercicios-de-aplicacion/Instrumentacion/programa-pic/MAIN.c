//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON


#pragma config LVP = OFF
#pragma config MCLRE = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"
#include "LibLCDXC8.h"

uint8_t USB_Out_Buffer[64];
char buffer[64], data[64];
unsigned char accion=0;
int escala=0, cont=0, n=0;
float info=0, DATA=0;

void USBTask(void);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);
void Configuracion(void);
float TEMP(void);

#define DATA_DIR TRISC1
#define DATA_IN RC1
#define DATA_OUT LATC1
unsigned char Temp,Hum,Che;
unsigned char Dec,Uni;
void LeerHT11(void);
unsigned char LeerByte(void);
unsigned char LeerBit(void);
unsigned char Check();

void __interrupt() TIMER(void);

void main(void) {
  
    SYSTEM_Initialize();//inicializacion de todo el sistema
    EscribeLCD_c('T');
    EscribeLCD_c('=');
    DireccionaLCD(0x94);
    EscribeLCD_c('H');
    EscribeLCD_c('=');
    while (1)
    {
        if(accion==1){
            info=0;
            LATB7=1;
            DireccionaLCD(0xD4);
            EscribeLCD_c(0x25);
            EscribeLCD_c('R');
            EscribeLCD_c('H');
            for(int j=0; j<3; j++){
                DATA=TEMP();
                info = info + DATA;
                __delay_ms(2);
            }
            info=info/3;
            LATB7=0;
            sprintf(buffer," %.0f,%d",info,Hum);
            accion=2;
            if(cont==1){
                sprintf(data," %.0f",info);
                n=strlen(data);
                DireccionaLCD(0x83);
                for(int i=0; i<n; i++){
                     EscribeLCD_c(data[i]);
                }
                for(int k=0; k<2; k++){
                     EscribeLCD_c(' ');
                }
                __delay_ms(1);
                sprintf(data," %d",Hum);
                DireccionaLCD(0x97);
                n=strlen(data);
                for(int i=0; i<n; i++){
                     EscribeLCD_c(data[i]);
                }
                for(int k=0; k<2; k++){
                     EscribeLCD_c(' ');
                }
                
                cont=0;
            }
        }
       USBTask(); 
       __delay_ms(2);
    }
}
void USBTask(void){
    uint8_t READ;
    
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexi�n
    
    READ = getsUSBUSART(USB_Out_Buffer,64);
    
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {
        if(READ != 0){
            
             switch(USB_Out_Buffer[0]){
                    case 0x41://A
                        escala=0;//celsius
                        break;
                    case 0x42://B
                        escala=1;//fareheit
                        break;
                    case 0x43://C
                        escala=2;//kelvin
                        break;
                    case 0x44://D
                        escala=3;//ranklein
                        break;
                    case 0x45://E
                        accion=1;
                        TMR0ON=1;
                        break;
                    case 0x46://F
                        accion=0;
                        TMR0ON=0;
                        break;
                    default:
                        escala=0;//celsius
                        break;
                }
        }
        if(accion==2){
            putrsUSBUSART(buffer); //escribir
            accion=3;
        }
           
    }
     CDCTxService(); //Procesa servicio USB
}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    ConfiguraLCD(4);
    InicializaLCD();
    USBDeviceInit();
    USBDeviceAttach();
}
void  INTERRUPT_Initialize (void){
    GIE = 1;//Activar interrupciones globales
    //TIMER0
    T0CON = 0b10000111;
    TMR0L = 0xE4; //1s
    TMR0H = 0x48; //1s
    TMR0IE = 1;//Activa interrupcion timer
    //TIMER1
    T1CON=0xF1;
    TMR1H=0xA0;
    TMR1L=0x15;
    TMR1IE=1;
    //USB
    PEIE=1;
    USBIE=1;
}
void Configuracion(void){
    TRISC0=0;//led timer
    TRISB=0x00;
    LATB7=0;
    LATC0=0;//iniciar leds
    TRISD=0;//LCD
    LATD=0;
    LATB0=0;
    DATA_OUT=0;
    //configuracion ADC
    ADCON1=15;//definir puertos digitales

}
float TEMP(void){
    float salida=0;
    __delay_ms(500);
    LATD0=1;
    __delay_ms(500);
    LATD0=0;
    LeerHT11();
    switch(escala){
        case 0:
            salida=Temp;
            DireccionaLCD(0xC0);
            EscribeLCD_c('C');
            EscribeLCD_c('e');
            EscribeLCD_c('l');
            EscribeLCD_c('s');
            EscribeLCD_c('i');
            EscribeLCD_c('u');
            EscribeLCD_c('s');
            EscribeLCD_c(' ');
            EscribeLCD_c(' ');
            EscribeLCD_c(' ');
            break;
        case 1:
            salida=(9*Temp/5)+32;
            DireccionaLCD(0xC0);
            EscribeLCD_c('F');
            EscribeLCD_c('a');
            EscribeLCD_c('h');
            EscribeLCD_c('r');
            EscribeLCD_c('e');
            EscribeLCD_c('n');
            EscribeLCD_c('h');
            EscribeLCD_c('e');
            EscribeLCD_c('i');
            EscribeLCD_c('t');
            break;
        case 2:
            salida=273.15+Temp;
            DireccionaLCD(0xC0);
            EscribeLCD_c('K');
            EscribeLCD_c('e');
            EscribeLCD_c('l');
            EscribeLCD_c('v');
            EscribeLCD_c('i');
            EscribeLCD_c('n');
            EscribeLCD_c(' ');
            EscribeLCD_c(' ');
            EscribeLCD_c(' ');
            EscribeLCD_c(' ');
            break;
        case 3:
            salida=(9*Temp/5)+491.67;
            DireccionaLCD(0xC0);
            EscribeLCD_c('R');
            EscribeLCD_c('a');
            EscribeLCD_c('n');
            EscribeLCD_c('k');
            EscribeLCD_c('i');
            EscribeLCD_c('n');
            EscribeLCD_c('e');
            EscribeLCD_c(' ');
            EscribeLCD_c(' ');
            EscribeLCD_c(' ');
            break;
        default:
            
            break;
    }
    return salida;
}
void LeerHT11(void){
  unsigned char i,contr=0;
  DATA_DIR=0;
  __delay_ms(18);
  DATA_DIR=1;
  while(DATA_IN==1);
  __delay_us(40);
  if(DATA_IN==0) contr++;
  __delay_us(80);
  if(DATA_IN==1) contr++;
  while(DATA_IN==1);
  Hum=LeerByte();
  LeerByte();
  Temp=LeerByte();
  LeerByte();
  Che=LeerByte();
}
unsigned char LeerByte(void){
  unsigned char res=0,i;
  for(i=8;i>0;i--){
    res=(res<<1) | LeerBit();  
  }
  return res;
}
unsigned char LeerBit(void){
  unsigned char res=0;
  while(DATA_IN==0);
  __delay_us(13);
  if(DATA_IN==1) res=0;
  __delay_us(22);
  if(DATA_IN==1){
    res=1;
    while(DATA_IN==1);
  }  
  return res;  
}
unsigned char Check(void){
  unsigned char res=0,aux;
  aux=Temp+Hum;
  if(aux==Che) res=1;
  return res;  
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0xE4; //1s
   		TMR0H = 0x48; //1s
        TMR0IF = 0;//desactiva bandera
        if(accion==3){
            accion=1;
            cont++;
        }
    }
    if (USBIF == 1) {
        USBIF=0;
        USBDeviceTasks();
        /*if(UEIR!=0){
           UEIR=0; 
        }
        if(UIR!=0){
           UIR=0; 
        }*/
    }
    if (TMR1IF == 1) {
        TMR1H=0;
        TMR1L=0;
        TMR1IF = 0;//desactiva bandera
        LATC0 = LATC0^1;//cambio estado del led
    }
}
