//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON
#pragma config LVP = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"

char USB_Out_Buffer[64];
char buffer[64];
char tecla[2];
int n=0;
char teclas[4][4]={{'A','3','2','1'},{'B','6','5','4'},{'C','9','8','7'},{'D','#','0','*'}};
void ProcessIO(void);
void USBTask(void);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);
void PIN_MANAGER_IOC(void);
void ConfiguracionPines(void);
char Teclado(void);
void __interrupt() TIMER(void);

void main(void) {
  
    SYSTEM_Initialize();//inicializacion de todo el sistema
    while (1)
    {
       LATB=0x0F;
       USBDeviceTasks();
       USBTask();
    }
}
void USBTask(void){
    uint8_t READ;
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexi�n
    
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {
        READ = getsUSBUSART(USB_Out_Buffer,64);
        if(READ != 0){
            if(USB_Out_Buffer[0]==0x42){//B
                LATA1=1;
            }
            if(USB_Out_Buffer[0]==0x52){//R
                LATA2=1;
            }
            if(USB_Out_Buffer[0]==0x59){//Y
                LATA3=1;
            }
            if(USB_Out_Buffer[0]==0x47){//G
                LATA4=1;
            }
            if(USB_Out_Buffer[0]==0x62){//b
                LATA1=0;
            }
            if(USB_Out_Buffer[0]==0x72){//r
                LATA2=0;
            }
            if(USB_Out_Buffer[0]==0x79){//y
                LATA3=0;
            }
            if(USB_Out_Buffer[0]==0x67){//g
                LATA4=0;
            }             
        }
        if(RE0==1){
            putrsUSBUSART("Estoy aqui \n");
            __delay_ms(400);
        }
        else{
            if(PORTB!= 0x0F){
               tecla[0]=Teclado();//dato a enviar
               putrsUSBUSART(tecla);
            }
        }
    }
    CDCTxService(); //Procesa servicio USB
}
void SYSTEM_Initialize(void){
    ConfiguracionPines();
    INTERRUPT_Initialize();
    USBDeviceInit();
}
void  INTERRUPT_Initialize (void){

    GIE = 1;//Activar interrupciones globales
     //TIMER0
    T0CON = 0b10000110; 
    TMR0L = 0x1A;
    TMR0H = 0x10;
    TMR0IE = 1;//Activa interrupcion timer
    TMR0ON = 1;//activa timer
    TMR0IF = 1;
}
void ConfiguracionPines(void){

    TRISB=0xF0;//TECLADO
    TRISA=0;//LEDS
    TRISC0=0;
    TRISE=0x01;//PULSADOR
    ADCON1=15;//PUERTOS DIGITALES
    LATA=0;//ESTADO INICIAL
    LATC0=0;
    LATB=0;
    LATE0=0;
}
char Teclado(void){
    int i,j;
    LATB=0x08;
    for(i=3;i>=0;i--){
        if(RB7==1){
            j=0;
            break;
        }
        if(RB6==1){
            j=1;
            break;
        }
        if(RB5==1){
            j=2;
            break;
        }
        if(RB4==1){
            j=3;
            break;
        }
        LATB=LATB>>1;
    }
    while((PORTB & 0xF0)!= 0x00);
    return teclas[j][i];
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0x1A;
        TMR0H = 0x10;
        TMR0IF = 0;//desactiva bandera
        LATC0 = LATC0^1;//cambio estado del led
    }
}
