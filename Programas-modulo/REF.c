//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON
#pragma config LVP = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"
#include "usb_device_cdc.h"

char USB_Out_Buffer[64];
char buffer[64];
void USBTask(void);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);
void PIN_MANAGER_IOC(void);
void Configuracion(void);
int strtoint(char *valor, int n);
void __interrupt() TIMER(void);

void main(void) {
    
    SYSTEM_Initialize();//inicializacion de todo el sistema
    
    while (1)
    {        
       USBDeviceTasks();
       USBTask(); 
       
    }
}
void USBTask(void){
    int num,t;
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexión
    uint8_t READ = getsUSBUSART(USB_Out_Buffer,64);
    t=strlen(USB_Out_Buffer);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {        
        if(READ != 0){
           num=strtoint(USB_Out_Buffer,t);//conversion string a entero
           if(num<16){
           CVRCON=(CVRCON & 0b11110000)|num;
           }
        }
    }
    CDCTxService(); //Procesa servicio USB
    
}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    USBDeviceInit();
}
void  INTERRUPT_Initialize (void){
    // Disable Interrupt Priority Vectors (16CXXX Compatibility Mode)
    RCONbits.IPEN = 0;
    GIE = 1;//Activar interrupciones globales
     //TIMER0
    T0CON = 0b10000110; 
    TMR0IE = 1;//Activa interrupcion timer
    TMR0ON = 1;//activa timer
 }
void PIN_MANAGER_IOC(void){   
	// Clear global Interrupt-On-Change flag
    INTCONbits.RBIF = 0;
}
void Configuracion(void){
//realizar configuración de registros TRIS
    TRISC0=0;//led timer
    LATC0=0;//iniciar led
    TRISA2=0;
    //Configuracion DAC
    CVRCON=0b11101111;//paso 208mV, rango 0V-3,335V
    
INTCON2bits.nRBPU = 1;
}
int strtoint(char *valor, int n){
    int i,resultado, conv;
    resultado=0;
    for(i=0;i<n;i++){
        conv=(int)valor[i];
        if((conv<=57)&&(conv>=48)){
            conv=(int)valor[i]-48;
            resultado=(resultado*10)+conv;
        }
        else{
            break;
        }
    }
    return resultado;
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0x1A;
        TMR0H = 0x10;
        TMR0IF = 0;//desactiva bandera
        LATC0 = LATC0^1;//cambio estado del led
    }
}
