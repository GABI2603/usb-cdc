//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON
#pragma config LVP = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include<math.h>
#include "usb.h"
#include "usb_device_cdc.h"

char USB_Out_Buffer[64];
char buffer[64];
char datos[14];
int cont=0;
unsigned char Lectura[20],regis=0, dato=0;
int s,mn,h,d,ms,a;
float info=0;
bool flag=0;
int active=0;
void ProcessIO(void);
void USBTask(void);
void INTERRUPT_Initialize (void);
void SYSTEM_Initialize(void);
float strtofloat(char *data);
void Configuracion(void);
void __interrupt() TIMER(void);
//I2C
void WriteI2C(unsigned char address,unsigned char registro, unsigned char data);
void ReadyI2C(void);
void StartI2C(void);
void SendI2C(unsigned char Data);
void StopI2C(void);
void ReadI2C(unsigned char address,unsigned char registr,int Nbytes,unsigned char *d);
void RepeatedStart(void);
unsigned char ReceiveI2C(char flag);

void main(void) {
    
    SYSTEM_Initialize();//inicializacion de todo el sistema
    active=0;
    while (1)
    {
       
       USBDeviceTasks();
       USBTask(); 
       
    }
}
void USBTask(void){
    unsigned char dec, unit;
    
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexión
    uint8_t READ = getsUSBUSART(USB_Out_Buffer,64);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {        
        if(READ != 0){
            if(active==2){
                active=0;
                info=strtofloat(USB_Out_Buffer);
                dec=(int)(info/10);
                unit=(int)info - (dec*10);
                dato=(dec<<4)|(unit &0x0F);
                WriteI2C(0xD0,regis,dato);
                putrsUSBUSART("Dato asignado \r\n");
            }
            if(active==1){
                active=2;
                switch(USB_Out_Buffer[0]){
                    case 0x41://A
                        regis=0x06;
                        break;
                    case 0x42://B
                        regis=0x05;
                        break;
                    case 0x43://C
                        regis=0x04;
                        break;
                    case 0x44://D
                        regis=0x02;
                        break;
                    case 0x45://E
                        regis=0x01;
                        break;
                    case 0x46://F
                        regis=0x00;
                        break;
                    default:
                        regis=0x00;
                        break;
                }
                putrsUSBUSART("Ingrese el valor a asignar: \r\n");
            }
            if((USB_Out_Buffer[0]==0x4C)&&(active==0)){//L
                ReadI2C(0xD0, 0x00, 7, Lectura);
                s=((Lectura[0]>>4)*10)+(Lectura[0]&0x0F);
                mn=((Lectura[1]>>4)*10)+(Lectura[1]&0x0F);
                h=(((Lectura[2]>>4)&0x3)*10)+(Lectura[2]&0x0F);
                d=((Lectura[4]>>4)*10)+(Lectura[4]&0x0F);
                ms=((Lectura[5]>>4)*10)+(Lectura[5]&0x0F);
                a=((Lectura[6]>>4) *10)+(Lectura[6] &0x0F);
                sprintf(buffer,"fecha: %d / %d / %d \n Hora: %d : %d : %d \n",d,ms,a,h,mn,s);
                putrsUSBUSART(buffer);
            }
            if((USB_Out_Buffer[0]==0x50)&&(active==0)){//P
                active=1;
                putrsUSBUSART("Selecciona el item a configurar: \r\n A.ano \n B.mes\n C.dia\n D.hora\n E.minuto\n F.segundo\n");            
            }
            
        }
    }

        CDCTxService(); //Procesa servicio USB

    
     

}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    CDCSetLineCoding(9600,NUM_STOP_BITS_1,PARITY_NONE,8);//configuracion
    USBDeviceInit();
    USBDeviceAttach();
}
void  INTERRUPT_Initialize (void){
    
    GIE = 1;//Activar interrupciones globales
     //TIMER0
    T0CON = 0b10000110; 
    TMR0L = 0x1A; 
    TMR0H = 0x10; 
    TMR0IE = 1;//Activa interrupcion timer
    TMR0ON = 1;//activa timer
    TMR0IF = 1;//Activar bandera
}
void PIN_MANAGER_IOC(void){   
	// Clear global Interrupt-On-Change flag
    INTCONbits.RBIF = 0;
}
void Configuracion(void){
//realizar configuración de registros TRIS
    TRISB=0x03;
    TRISC0=0;
    LATC0=0;
    ADCON1=15;
    SSPADD=119;//95;
    SSPSTAT=0x80;
    SSPCON1=0x28;
    SSPCON2=0x00;
//INTCON2bits.nRBPU = 1;
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {
        TMR0L = 0x1A; 
        TMR0H = 0x10; 
        TMR0IF = 0;//desactiva bandera
        LATC0 = LATC0^1;//cambio estado del led
    }
}
void WriteI2C(unsigned char address,unsigned char registro, unsigned char data){
    StartI2C();
    SendI2C(address);
    SendI2C(registro);
    SendI2C(data);
    StopI2C();
    __delay_ms(10);
}
void StartI2C(void){
    ReadyI2C();
    SEN = 1;
    
}
void ReadyI2C(void){
    while((SSPCON2 & 0x1F)||(SSPSTAT & 0x04)){}
}
void SendI2C(unsigned char Data){
    ReadyI2C();
    SSPBUF = Data;
}
void StopI2C(void){
    ReadyI2C();
    PEN = 1;
    
}
void ReadI2C(unsigned char Address, unsigned char Registr, int Nbytes, unsigned char *d){
    int i;
    StartI2C();
    SendI2C(Address);
    SendI2C(Registr);
    RepeatedStart();
    SendI2C(Address|0x01);
    for (i=0; i<Nbytes; i++){
        if(i+1 == Nbytes){
            *d = ReceiveI2C(1);
        }else{
            *d = ReceiveI2C(0);
        }
        d++;
    }
    StopI2C();
    __delay_ms(10);
}
void RepeatedStart(void){
    ReadyI2C();
    RSEN = 1;
}
unsigned char ReceiveI2C(char flag){
    unsigned char buffer;
    ReadyI2C();
    RCEN = 1;
    ReadyI2C();
    buffer = SSPBUF;
    ReadyI2C();
    ACKDT = (flag == 1)? 1 : 0;
    ACKEN = 1;
    return buffer;
}
float strtofloat(char *data){
    float resultado;
    int i, conv, n, punto=1,cont=0, decimales=0, enteros=0;
    n=strlen(data);
    for(i=0;i<n;i++){
        conv=(int)data[i];
        if((conv<=57)&&(conv>=48)){
            if(punto==1){
            conv=(int)data[i]-48;
            enteros=(enteros*10)+conv;
            }
            else{
                conv=(int)data[i]-48;
                decimales=(decimales*10)+conv;
                cont++;
            }
        }
        else{
            if((conv==46)&&(punto==1)){
                punto=0;
            }
            else{
                break;
            }  
        }
    }
    resultado=(float)enteros;
    return resultado;
}
