//configuracion de cristal y frecuencia
#pragma config WDT = OFF          
#pragma config CPUDIV = OSC1_PLL2      
#pragma config FOSC = HSPLL_HS //20MHz
#pragma config PLLDIV = 5
#pragma config XINST = OFF
#pragma config USBDIV = 2
#pragma config VREGEN = ON

#pragma config LVP = OFF
#pragma config MCLRE = OFF

#define _XTAL_FREQ 48000000

#include <xc.h>
#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include "usb.h"

int ref=20;
int cont=0, lectura=0,error=0;
void SYSTEM_Initialize(void);
void Configuracion(void);
void  INTERRUPT_Initialize(void);
float referencia(void);
void USBTask(void);
bool flag=0, time=0;
uint8_t USB_Out_Buffer[64];
char buffer[64];

void Fuzzificacion(float Temp);
void AnalisisReglas(void);
float Defuzzificacion(void);
float SetFrio(float Temp);
float SetTibio(float Temp);
float SetCaliente(float Temp);

float uf,ut,uc,r1,r2,r3,sal;
int seleccion=0;

float TEMP(void);

#define DATA_DIR TRISC1
#define DATA_IN RC1
#define DATA_OUT LATC1
unsigned char Temp,Hum,Che;
unsigned char Dec,Uni;
void LeerHT11(void);
unsigned char LeerByte(void);
unsigned char LeerBit(void);
unsigned char Check();

void main(void) {
     SYSTEM_Initialize();//inicializacion de todo el sistema
     while(1){
        LATB7=1;
        lectura=TEMP();
        LATB7=0;
        ref=referencia();//Lectura potenciomentro
        error=lectura-ref;//calculo del error
        Fuzzificacion(error);
        //se calcula el valor de activaci�n de cada regla
        AnalisisReglas();
        //se calcula el valor de salida por el metodo de momento medio MOM
        sal=10 * Defuzzificacion();//la fuzificacion se multiplica por 10 para conocer el valor en el cual se activa el pulso
        seleccion=(int)sal;
        sprintf(buffer,"%.2f,%.2f,%.2f",lectura,referencia,error);
        __delay_ms(1000);
        USBTask();
     }
}
void SYSTEM_Initialize(void){
    Configuracion();
    INTERRUPT_Initialize();
    USBDeviceInit();
    USBDeviceAttach();
}
void Configuracion(void){
    TRISD7=0;//SALIDA RELE
    TRISD0=0;
    TRISC0=0;//led timer
    TRISB0=1;
    TRISB7=0;
    LATB7=0;
    LATD7=0;//BOMBILLO apagado
    LATC0=0;//iniciar led
    LATD0=0;
    DATA_OUT=0;
    //configuracion ADC
    ADCON1=0x0D;//definir puertos analogos
    ADCON2=0xA2;//conversor analogo-digital

}
void  INTERRUPT_Initialize (void){
    GIE = 1;//Activar interrupciones globales
    //TIMER0
    T0CON = 0b10000101; 
    TMR0L = 0xC8; 
    TMR0H = 0xFE; 
    TMR0IE = 1;//Activa interrupcion timer
    //int0
    PEIE=1;//INTERRUPCION PERIFERICA
    INT0IE=1;//INTERRUPCION INT0

    //USB
    USBIE=1;
}
float referencia(void){
    unsigned int valor;
    float salida;
    //lectura ADC
    ADCON0=0x03;
    while(ADCON0bits.GO_nDONE == 1);
    valor = ((ADRESH<<8)|ADRESL);
    //CONVERSION 
    salida = (valor*20/1023)+20;
    return salida;   
}
void USBTask(void){
    uint8_t READ;
    if(USBGetDeviceState() < CONFIGURED_STATE || (USBIsDeviceSuspended() == true)) return;//comprobar la conexi�n
    READ = getsUSBUSART(USB_Out_Buffer,64);
    if(USBUSARTIsTxTrfReady()) //Si el Buffer de salida esta libre
    {
        if(READ != 0){
            if(USB_Out_Buffer[0]=='A'){
                sprintf(buffer," %d %d %d ", lectura,seleccion,ref);
                putrsUSBUSART(buffer);
            }
        }
        
        
    }
    CDCTxService(); //Procesa servicio USB
}
float SetFrio(float Temp){
  //Funci�n que determina el grado de pertenencia al conjunto frio
  float grado;
  if(Temp<-15){
    grado=1;    
  }else if(Temp<0){
    grado= -(Temp/15);  
  }else{
    grado=0;  
  }
  return grado;    
}
float SetTibio(float Temp){
  //Funci�n que determina el grado de pertenencia al conjunto tibio
  float grado;
  if(Temp<-15){
    grado=0;    
  }else if(Temp<0){
    grado=(Temp/15)+ 1;  
  }else if(Temp<3){
    grado=1-(Temp/3);  
  }else{
    grado=0;  
  }
  return grado;    
}
float SetCaliente(float Temp){
  //Funci�n que determina el grado de pertenencia al conjunto caliente
  float grado;
  if(Temp<0){
    grado=0;    
  }else if(Temp<3){
    grado=(Temp/3);  
  }else{
    grado=1;  
  }
  return grado;    
}
void Fuzzificacion(float Temp){
  //Funci�n que concentra el calculo de los grados de pertenencia
  uf=SetFrio(Temp);
  ut=SetTibio(Temp);
  uc=SetCaliente(Temp);
}
void AnalisisReglas(void){
  //Funci�n que calcula el valor de activaci�n de las reglas
  r1=0;
  r2=0;
  r3=0;  
  if(uf>0)
    r1=uf;
  if(ut>0)
    r2=ut;
  if(uc>0)
    r3=uc;  
}
float Defuzzificacion(void){//los valores de defizificacion varian entre 0 y 1 que al ser multiplicados por 10 da un valor entero entre 0 y 10
  //Funci�n que calcula el valor de salida apartir de los centroides de los conjuntos de salida
  float sal=0;
  if(r1>0)
    sal=sal+0.09*r1;
  if(r2>0)
    sal=sal+0.772*r2;
  if(r3>0)
    sal=sal+1*r3;
  return sal;
}
float TEMP(void){
    float salida=0;
    __delay_ms(500);
    LATD0=1;
    __delay_ms(500);
    LATD0=0;
    LeerHT11();
    salida=Temp;
    return salida;
}
void LeerHT11(void){
  unsigned char i,contr=0;
  DATA_DIR=0;
  __delay_ms(18);
  DATA_DIR=1;
  while(DATA_IN==1);
  __delay_us(40);
  if(DATA_IN==0) contr++;
  __delay_us(80);
  if(DATA_IN==1) contr++;
  while(DATA_IN==1);
  Hum=LeerByte();
  LeerByte();
  Temp=LeerByte();
  LeerByte();
  Che=LeerByte();
}
unsigned char LeerByte(void){
  unsigned char res=0,i;
  for(i=8;i>0;i--){
    res=(res<<1) | LeerBit();  
  }
  return res;
}
unsigned char LeerBit(void){
  unsigned char res=0;
  while(DATA_IN==0);
  __delay_us(13);
  if(DATA_IN==1) res=0;
  __delay_us(22);
  if(DATA_IN==1){
    res=1;
    while(DATA_IN==1);
  }  
  return res;  
}
unsigned char Check(void){
  unsigned char res=0,aux;
  aux=Temp+Hum;
  if(aux==Che) res=1;
  return res;  
}
void __interrupt() TIMER(void){
    if (TMR0IF == 1) {//el timer tiene un tiempo de 1/10 del periodo de la red 
        if(seleccion<10){//si el valor de seleccion es 10 (el maximo) el bombillo permanece apagado
            if(cont==seleccion){//define en que pulso se enciende el bombillo
                LATD7=1;
            }
        }
        TMR0L = 0xC8; 
        TMR0H = 0xFE;
        TMR0IF = 0;//desactiva bandera
        LATC0=LATC0^1;
        cont++;//se lleva el conteo de interrupciones
    }
    
     if(INT0IF == 1){//lee el valor enviado por la tarjeta
        INT0IF=0;//desactivar la bandera
        
        flag = flag ^ 1 ;//se determina la segunda vez que la se�al pasa por 0
        if (flag==1){//cuando la se�al pasa por 0 por primera vez
            LATD0=LATD0^1;
            TMR0IF = 1;//se activa la bandera del timer
            cont=0;//se reinicia el conteo
            if(seleccion != 0){//si seleccion =0 el bombillo esta encendido todo el tiempo
                LATD7=0;
            }
        }
    }
    if (USBIF == 1) {
        USBIF=0;
        USBDeviceTasks();
        if(UEIR!=0){
           UEIR=0; 
        }
        if(UIR!=0){
           UIR=0; 
        }
    }
}
