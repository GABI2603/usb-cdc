function varargout = INST(varargin)
% INST MATLAB code for INST.fig
%      INST, by itself, creates a new INST or raises the existing
%      singleton*.
%
%      H = INST returns the handle to a new INST or the handle to
%      the existing singleton*.
%
%      INST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INST.M with the given input arguments.
%
%      INST('Property','Value',...) creates a new INST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before INST_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to INST_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help INST

% Last Modified by GUIDE v2.5 16-Aug-2021 19:14:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @INST_OpeningFcn, ...
                   'gui_OutputFcn',  @INST_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before INST is made visible.
function INST_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to INST (see VARARGIN)

% Choose default command line output for INST
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
clc
delete(instrfindall);
s = serial('COM4','Baudrate',9600,'Databits',8,'Parity','None','StopBits',1,'FlowControl','none');
set(s,'timeout',0.1);
s.BytesAvailableFcnCount=1;
s.BytesAvailableFcnMode='byte';
s.BytesAvailableFcn={@pushbutton9_Callback,handles};
fopen(s);
assignin('base','puerto',s);
global a;
global x;
global X;
global Y;
global data;
data =[0 0]';
X = [0];
Y = [0];
a=0;
x=0;

% UIWAIT makes INST wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = INST_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
s=evalin('base','puerto');
fwrite(s,'A');
global X;
global Y;
global x;
global veces;
veces=0;
x=0;
set(handles.text2,'String','T(C)');
dato = fscanf(s);
y = str2double(dato);
X = [0];
Y = [0];


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

s=evalin('base','puerto');
fwrite(s,'B');
global X;
global Y;
global x;
x=0;
set(handles.text2,'String','T(F)');
dato = fscanf(s);
y = str2double(dato);
X = [0];
Y = [0];
global veces;
veces=0;
% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
s=evalin('base','puerto');
fwrite(s,'C');
global X;
global Y;
global x;
x=0;
set(handles.text2,'String','T(K)');
dato = fscanf(s);
y = str2double(dato);
X = [0];
Y = [0];
global veces;
veces=0;

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
s=evalin('base','puerto');
fwrite(s,'D');
global X;
global Y;
global x;
x=0;
set(handles.text2,'String','T(R)');
X = [0];
Y = [0];
global veces;
veces=0;
% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global a;
a=0;
s=evalin('base','puerto');
fwrite(s,'F');
opc=questdlg('Desea salir?','SALIR','Si','No','No');
if(strcmp(opc,'No'))
    return
end
fclose(s);
%delete(instrfindall);
clear all, clc, close all

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global a;
global data;
a=0;
s=evalin('base','puerto');
fwrite(s,'F');
%xlswrite('datos',data, 'temperatura2','B2');
xlswrite('datos',data, 'temperatura2','B2');


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
s=evalin('base','puerto');
fwrite(s,'E');
global X;
global Y;
global a;
global x;
global veces;
veces=0;
dato = fscanf(s);
y = str2double(dato);
x=0;
a=1;
X = [0];
Y = [y];

function pushbutton9_Callback(obj,~,handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
s=evalin('base','puerto');
warning('off','MATLAB:serial:fscanf:unsuccessfulRead');
BytesAvailable=obj.BytesAvailable;
ValuesReceived=obj.ValuesReceived;

warning('off','all');
global x;
global a;
global X;
global Y;
global data;
global i;
global veces;
ndatos = 900;   %15min
if a == 1
    dato = fscanf(obj);
    y = str2double(dato);
    X =[X x];
    Y =[Y y];
    
    i = x - (ndatos*veces) + 1;

    data(i,1)= x; 
    data(i,2)= y; 
    x = x+1;
    b = size(Y);
    if b(2) > 10
        X(1)=[];
        Y(1)=[];
    end
    plot(handles.axes1,X,Y,'Color','b','LineWidth', 1.5); 
    if mod(x,ndatos) == 0
        xlswrite('datos',data, 'temperatura','B2');
        data=[];
        veces= veces +1;
    end
    pause(0.7);
else
    fwrite(s,'F');
    
end
